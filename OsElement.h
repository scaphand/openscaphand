#ifndef OSELEMENT_H
#define OSELEMENT_H

#include <vector>
#include <string>

#include "Node.h"

class gkScene;
class gkGameObject;

#include <vrj/Kernel/User.h>

/*namespace gadget
{
    class PositionInterface;
}*/

using namespace std;

/** If a VRJuggler element exist for this node, it take precedence to the Ogrekit GameObject position
Else the Ogrekit GameObject is use to get the element position

The root object, "os" don't have to be declared in blender. It's the default root Node and it's position
is the origin
*/

class OsElement : public Node
{
    public:
        OsElement(OsElement* parent, string name, string path = "", gkScene* scene = 0);
        virtual ~OsElement();
        static void loadElements(gkScene* scene);

        bool isCamera();

        bool IsRootNode();

        static OsElement* getRoot();

    protected:

        gmtl::Matrix44f getTransform();
        bool isTracked();
        virtual void Update();

    private:
        static OsElement* root; // The root "os" node
        gkScene* scene;
        gkGameObject* gameObject;
        gadget::PositionInterface*  vrjInterface;
        gmtl::Matrix44f transformMatrix;
};

#endif // OSELEMENT_H
