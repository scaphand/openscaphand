#include "OsElement.h"

#include <vrj/Kernel/User.h>

#include "OgreKit.h"

#include "OgreUtils.h"

#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>

using namespace boost;

OsElement* OsElement::root = new OsElement(0, "os");

OsElement::OsElement(OsElement* parent, string name, string path, gkScene* scene) : Node(parent, name)
{
    this->scene = scene;

    /// Set the VRJuggler element (if any)
    if (gadget::InputManager::instance()->getProxy(name))
    {
        vrjInterface = new gadget::PositionInterface();
        vrjInterface->init(name);
    }
    else
    {
        vrjInterface = 0;
    }

    /// Set the Ogrekit object
    if (scene)
    {
        gameObject = scene->getObject(path);
    }
    else
    {
        gameObject = 0;
    }
}

OsElement::~OsElement()
{
    delete vrjInterface;
}

void OsElement::loadElements(gkScene* scene)
{
    /// Get list of all OpenScaphand elements and sort them to load them in ordered map
    map<string, gkGameObject*> elementStringList;
    gkGameObjectHashMap::Iterator iterOgreKitElement = scene->getObjects();
    while (iterOgreKitElement.hasMoreElements())
    {
        gkGameObject* object = iterOgreKitElement.getNext().second;
        if (object->getName().substr(0, 3) == "os.")
        {
            elementStringList[object->getName()] = object;
        }
    }

    map<string, gkGameObject*>::iterator iterOs;
    for (iterOs = elementStringList.begin(); iterOs != elementStringList.end(); iterOs++)
    {
        gkGameObject* object = iterOs->second;

        char_separator<char> separator(".");
        tokenizer<char_separator<char> > tokens(object->getName(), separator);
        vector<string> tokensVector;
        BOOST_FOREACH (const string& token, tokens)
        {
            tokensVector.push_back(token);
        }

        if (tokensVector.size() == 2)
        {
            OsElement::getRoot()->AddChildNode(new OsElement(OsElement::getRoot(), tokensVector.back(), object->getName(), scene));
        }
        else if (tokensVector.size() > 2)
        {
            OsElement *parentNode = static_cast<OsElement*>(OsElement::getRoot()->FindChildNode(tokensVector[tokensVector.size()-2]));
            if (parentNode)
            {
                parentNode->AddChildNode(new OsElement(parentNode, tokensVector.back(), object->getName(), scene));
            }
        }
    }
}

gmtl::Matrix44f OsElement::getTransform()
{
    return transformMatrix;
}

void OsElement::Update()
{

    if (!this->IsRootNode()) /// Root "os" is always identity matrix
    {

        /// Get parent transformation matrix
        gmtl::Matrix44f parentPosition = (static_cast<OsElement*>(this->GetParentNode()))->getTransform();

        /// Get local transformation matrix
        gmtl::Matrix44f localPosition;
        if (this->isTracked() && (this->scene->getMainCamera() != gameObject)) /// is tracked by a VRJuggler interface
        {
            localPosition = (*vrjInterface)->getData();
        }
        else if (gameObject && (this->scene->getMainCamera() != gameObject)) /// is tracked by OgreKit object
        {
            gkMatrix4 localOgreKitPosition = gameObject->getTransform();
            localPosition = MatrixUtils::fromOGRE(localOgreKitPosition);
        }

        /// Update the transformation
        this->transformMatrix = parentPosition * localPosition;

        /// Apply the transformation to the Ogrekit object
        if (gameObject)
        {
            gameObject->setTransform(MatrixUtils::fromGMTL(this->transformMatrix));
        }
    }
}

bool OsElement::isCamera()
{
    if (gameObject)
    {
        return (gameObject->getCamera() != 0);
    }
    else
    {
        return false;
    }
}

bool OsElement::isTracked()
{
    return (vrjInterface != 0);
}

bool OsElement::IsRootNode()
{
    return (this == root);
}

OsElement* OsElement::getRoot()
{
    return root;
}
