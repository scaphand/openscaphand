#include "Node.h"

Node::Node(Node* Parent,  std::string Name)
    : m_Name(Name)
{
    m_Parent = Parent;
}

Node::~Node(void)
{
    m_Parent = 0;
    m_Children.clear();
}

void Node::Traverse(void)
{
    Update();

    if(!m_Children.empty())
    {
        for(std::size_t i = 0; i < m_Children.size(); ++i)
        {
            if(0 != m_Children[i])
            {
                m_Children[i]->Traverse();
            }
        }
    }
}

Node* Node::GetParentNode(void)
{
    return(m_Parent);
}

void Node::SetParentNode(Node* NewParent)
{
    if(0 != m_Parent)
    {
        m_Parent->RemoveChildNode(this);
    }
    m_Parent = NewParent;
}

void Node::AddChildNode(Node* ChildNode)
{
    if(0 != ChildNode)
    {
        if(0 != ChildNode->GetParentNode())
        {
            ChildNode->SetParentNode(this);
        }
        m_Children.push_back(ChildNode);
    }
}

void Node::RemoveChildNode(Node* ChildNode)
{
    if(0 != ChildNode && !m_Children.empty())
    {
        for(std::size_t i = 0; i < m_Children.size(); ++i)
        {
            if(m_Children[i] == ChildNode)
            {
                m_Children.erase(m_Children.begin() + i);
                break; // break the for loop
            }
        }
    }
}

 std::string Node::GetNodeName(void)
{
    return(m_Name);
}

 std::size_t Node::CountChildNodes( bool RecursiveCount)
{
    if(!RecursiveCount)
    {
        return(m_Children.size());
    }
    else
    {
        std::size_t Retval = m_Children.size();
        for(std::size_t i = 0; i < m_Children.size(); ++i)
        {
            Retval += m_Children[i]->CountChildNodes(true);
        }
        return(Retval);
    }
}

Node* Node::GetChildNodeByName( std::string SearchName)
{
    Node* Retval = 0;
    if(!m_Children.empty())
    {
        for(std::size_t i = 0; i < m_Children.size(); ++i)
        {
            if(0 == m_Children[i]->m_Name.compare(SearchName))
            {
                Retval = m_Children[i];
                break; // break the for loop
            }
        }
    }
    return(Retval);
}

Node* Node::FindChildNode(std::string SearchName)
{
    Node* Retval = 0;
    if(!m_Children.empty())
    {
        for(std::size_t i = 0; i < m_Children.size(); ++i)
        {
            if(0 == m_Children[i]->m_Name.compare(SearchName))
            {
                Retval = m_Children[i];
                break; // break the for loop
            }

            Node* childSearchResult = m_Children[i]->FindChildNode(SearchName);
            if (childSearchResult)
            {
                Retval = childSearchResult;
                break;
            }
        }
    }
    return(Retval);
}
