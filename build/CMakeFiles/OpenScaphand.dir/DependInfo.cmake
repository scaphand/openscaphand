# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "C:/vr/OpenScaphand/App.cpp" "C:/vr/OpenScaphand/build/CMakeFiles/OpenScaphand.dir/App.cpp.obj"
  "C:/vr/OpenScaphand/Node.cpp" "C:/vr/OpenScaphand/build/CMakeFiles/OpenScaphand.dir/Node.cpp.obj"
  "C:/vr/OpenScaphand/OgreUtils.cpp" "C:/vr/OpenScaphand/build/CMakeFiles/OpenScaphand.dir/OgreUtils.cpp.obj"
  "C:/vr/OpenScaphand/OsElement.cpp" "C:/vr/OpenScaphand/build/CMakeFiles/OpenScaphand.dir/OsElement.cpp.obj"
  "C:/vr/OpenScaphand/main.cpp" "C:/vr/OpenScaphand/build/CMakeFiles/OpenScaphand.dir/main.cpp.obj"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "../../vrjuggler/modules/vrjuggler"
  "../../vrjuggler/modules/vapor"
  "../../vrjuggler-deps/cppdom/cppdom"
  "C:/vr/vrjuggler-deps/boost_1_53_0"
  "../../vrjuggler/modules/jackal"
  "../../vrjuggler/modules/sonix"
  "../../vrjuggler/modules/gadgeteer"
  "../../vrjuggler-deps/gmtl"
  "../../vrjuggler/modules/jackal/rtrc"
  "../../vrjuggler/modules/jackal/common"
  "../../vrjuggler/modules/jackal/config"
  "../../gamekit/Engine"
  "../../gamekit/Engine/AI"
  "../../gamekit/build-mingw/Engine"
  "../../gamekit/Ogre-1.9a/OgreMain/include"
  "../../gamekit/build-mingw/include"
  "../../gamekit/Dependencies/Source/GameKit/Utils"
  "../../gamekit/bullet/src"
  "../../gamekit/Dependencies/Source/GameKit/AnimKit"
  "../../gamekit/Dependencies/Source/OIS/include"
  "../../gamekit/Dependencies/Source/Lua/lua"
  "../../gamekit/Dependencies/Source/OpenAL"
  "../../gamekit/Ogre-1.9a/Components/Overlay/include"
  "../../gamekit/Ogre-1.9a/RenderSystems/GL/include"
  "../../glew/include"
  "../../OgreKitRenderToTexture"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
