/*************** <auto-copyright.pl BEGIN do not edit this line> **************
 *
 * VR Juggler is (C) Copyright 1998-2010 by Iowa State University
 *
 * Original Authors:
 *   Allen Bierbaum, Christopher Just,
 *   Patrick Hartling, Kevin Meinert,
 *   Carolina Cruz-Neira, Albert Baker
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 *
 *************** <auto-copyright.pl END do not edit this line> ***************/


#include <GL/glew.h>

#include "OgreKit.h"

#include <cstdlib>
#include "App.h"

#include <vrj/Kernel/Kernel.h>

#include <stdlib.h>

using namespace vrj;


int main(int argc, char** argv)
{
    char* val = getenv("VJ_BASE_DIR");
    if (!val)
    {
        putenv("VJ_BASE_DIR=.\\vrjuggler");
    }

	TestMemory;

	vrj::Kernel* kernel = vrj::Kernel::instance();  // Get the kernel

	kernel->init(argc, argv);

	kernel->loadConfigFile("gamekit.jconf");

	kernel->start();

    std::string gamekitBlendFile("test.blend");
    if (argc > 1) gamekitBlendFile = argv[1];

	vrjgk::App* application = new vrjgk::App(kernel, gamekitBlendFile);

	kernel->setApplication(application);
	kernel->waitForKernelStop();

	delete application;

	return 0;
}

