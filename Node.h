// Simple Scene Graph from : http://scriptionary.com/2009/02/17/simple-scene-graph-in-c/
// With minor changes...

#ifndef NODE_H
#define NODE_H

#include <vector>
#include <string>

// TODO : Change capitalization
// TODO : Implement visitor pattern

class Node
{
public:
    Node(Node* Parent = 0,  std::string Name = 0);
    virtual ~Node(void);

    void Traverse(void); // Call Update on this node and it's children

    Node* GetParentNode(void) ;
    void SetParentNode(Node* NewParent);

    void AddChildNode(Node* ChildNode);
    void RemoveChildNode(Node* ChildNode);

    std::string GetNodeName(void) ;
    std::size_t CountChildNodes( bool RecursiveCount = false);
    virtual  bool IsRootNode(void)  = 0;

    Node* GetChildNodeByName(std::string SearchName);

    Node* FindChildNode(std::string SearchName);
protected:
    virtual void Update(void) = 0;
private:
    Node* m_Parent;
    std::string m_Name;
    std::vector<Node*> m_Children;

};

#endif // NODE_H
