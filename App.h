#ifndef APP_H
#define APP_H

#ifndef _SIGSET_T_
#define	_SIGSET_T_
typedef int	_sigset_t;
#endif	/* Not _SIGSET_T_ */

#include <GL/glew.h>

//#include <vrj/vrjConfig.h>

#include <vpr/Sync/Mutex.h>
//#include <vpr/Sync/Guard.h>

#include <vrj/Draw/OpenGL/App.h>
//#include <vrj/Draw/OpenGL/ContextData.h>
//#include <vrj/Draw/OpenGL/Window.h>

//#include <vrj/Display/CameraProjection.h>

//#include <vrj/Kernel/Kernel.h>
//#include <vrj/Kernel/User.h>

#include "OgreKit.h"

class OsElement;


namespace vrjgk
{

class App : public vrj::opengl::App, public gkCoreApplication, public gkWindowSystem::Listener
{
public:
	App(vrj::Kernel* kern = NULL, std::string gamekitBlendFile = "test.blend");

	virtual ~App();

	void initScene();

	/** For gkCoreApplication (default return false, wich crash the app) */
	virtual bool setup();

	virtual void configSceneView(gkScene* scene);

   virtual void latePreFrame();

   virtual void preFrame();

   virtual void draw();

   virtual void init();

   void contextInit();

   virtual void contextClose();

   virtual void contextPreDraw();

   virtual void bufferPreDraw();

   virtual void pipePreDraw();

   virtual float getDrawScaleFactor()
   {
      return gadget::PositionUnitConversion::ConvertToMeters;
   }

   virtual void exit();



protected:

   virtual void update();

   virtual void loadScene();

	gkScene* scene;

protected:

    void assocGamekitObjectToVRJObject();

    void renderOneFrame();

    std::string gamekitBlendFile;

    std::vector<std::string> mappedObjects;
    std::vector<OsElement*>* osElementList;
    bool isInit;
    double time;
    double lastFrameTime;

private:

   vpr::Mutex mSceneViewLock;
};

}

#endif // APP_H
