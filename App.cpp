#include "App.h"

/// OpenScaphand related include
#include "OsElement.h"

/// VRJuggler related includes
#include <vrj/vrjConfig.h>
#include <vrj/Draw/OpenGL/ContextData.h>
#include <vrj/Draw/OpenGL/Window.h>
//#include <vrj/Kernel/User.h>

/// OgreKit related includes
#include "OgreKit.h"

/// Ogre 3D related includes
#include <OgreCamera.h>
#include <OgreLog.h>
#include <OgreRenderWindow.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreRenderSystem.h>
#include <OgreQuaternion.h>
#include <OgreGLRenderSystem.h>
#include <OgreMeshManager.h>
#include <OgreMovablePlane.h>
#include <OgreEntity.h>
#include <OgreHardwarePixelBuffer.h>

/// Utility for converting GMTL matrix to Ogre 3D
#include "OgreUtils.h"

namespace vrjgk
{

Ogre::String const ogreRenderSystem("OpenGL Rendering Subsystem");

App::App(vrj::Kernel* kern, std::string gamekitBlendFile) : vrj::opengl::App(kern), gkCoreApplication()
{
    this->scene = 0;
    this->gamekitBlendFile = gamekitBlendFile;
    this->isInit = false;
    this->osElementList = new std::vector<OsElement*>();
}

App::~App()
{
    for(std::vector<OsElement*>::iterator iter = osElementList->begin(); iter != osElementList->end(); ++iter)
    {
        delete *iter;
    }
    delete osElementList;
}

bool App::setup()
{
    return true;
}

void App::initScene()
{

}

void App::configSceneView(gkScene* scene)
{
}

void App::latePreFrame()
{

}

void App::preFrame()
{
    update();
}

void App::init()
{
    vrj::opengl::App::init();

    //Create the scene
    this->initScene();
}

void App::exit()
{

}

void App::contextClose()
{
    m_engine->requestExit();
    delete m_engine;
    m_engine = 0;
}

void App::contextPreDraw()
{

}

void App::bufferPreDraw()
{
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void App::pipePreDraw()
{

}

/// Called by preframe
void App::update()
{
    if (this->scene)
    {
        OsElement::getRoot()->Traverse(); /// Update all node
    }
}

void App::renderOneFrame()
{
	m_engine->m_root->renderOneFrame();
}

const gkString gkDefaultConfig  = "OgreKitStartup.cfg";

void App::contextInit()
{
    HWND windowHandle = FindWindow(NULL,"SimWindow");

    m_prefs.extWinhandle = Ogre::StringConverter::toString((unsigned int)windowHandle);
    m_prefs.grabInput = true;
    m_prefs.debugFps = true;
    m_prefs.shadowtechnique = "none";
    m_prefs.useBulletDbvt = false;
    gkString ress = m_prefs.resources;
    gkLogMessage(m_prefs.resources);
    assert(initialize());

    loadScene();

    assocGamekitObjectToVRJObject();

    assert(m_engine->initializeStepLoop());
    isInit = true;
}

/// Is called once by viewport
void App::draw()
{
    vrj::opengl::DrawManager* dm      = dynamic_cast<vrj::opengl::DrawManager*>(getDrawManager());
    vrj::opengl::UserData*    ud      = dm->currentUserData();

    vrj::ProjectionPtr  projection = ud->getProjection();
    const vrj::Frustum& frustum    = ud->getProjection()->getFrustum();

    Ogre::Matrix4 viewMatrix = MatrixUtils::fromGMTL(projection->getViewMatrix());
    Ogre::Matrix4 projMatrix = MatrixUtils::makeFrustumMatrix(frustum);

    gkGameObject* mainCamera = scene->getMainCamera();
    if (mainCamera)
    {
        gkVector3 position = mainCamera->getPosition();
        Ogre::Quaternion orientation = mainCamera->getOrientation();

        Ogre::Matrix4 inverseViewMatrix = viewMatrix.inverseAffine();
        Ogre::Matrix4 userTranslation;
        userTranslation.makeTrans(inverseViewMatrix.getTrans());
        Ogre::Matrix4 userRotation(inverseViewMatrix.extractQuaternion());
        Ogre::Matrix4 ogrekitTranslation;
        ogrekitTranslation.makeTrans(position);
        Ogre::Matrix4 ogrekitRotation(orientation);

        Ogre::Matrix4 transMatrix = ogrekitTranslation * ogrekitRotation * userTranslation * userRotation;
        viewMatrix = transMatrix.inverseAffine();
    }

    m_engine->getActiveScene()->getMainCamera()->getCamera()->setCustomViewMatrix(true, viewMatrix);
    m_engine->getActiveScene()->getMainCamera()->getCamera()->setCustomProjectionMatrix(true, projMatrix);

    renderOneFrame();
}

void App::loadScene()
{
    gkBlendFile* blend = gkBlendLoader::getSingleton().loadFile(gkUtils::getFile(gamekitBlendFile), gkBlendLoader::LO_ALL_SCENES);
    assert(blend);

    scene = blend->getMainScene();

    assert(scene);

    scene->createInstance();
}

void App::assocGamekitObjectToVRJObject()
{
    OsElement::loadElements(this->scene);
}

}
